#!/bin/bash
REPORT=/ngt-test-qa/ngt-test-qa/reports/json-report
FILE=$REPORT/result.json
DATE=$(date +"%F")

# Validate & push the metrics
if [ -f $FILE ]; then
    echo "File $FILE exists."
    cat $REPORT/result.json | \
    # Extract a nice json from last report
    #jq -r '.projectName as $project |.environment as $env | .specResults[] |"ngt_test_qa {env=\"\($env)\", spec=\"\(.specHeading|gsub("\"";"\\\""))\", result=\"\(.executionStatus)\"} \(.executionTime)"' | \
    jq -r '.projectName as $project |.environment as $env | .specResults[] |.specHeading as $spec| .scenarios[] |.scenarioHeading as $scen|.items[]| select(.itemType=="step") |.stepText as $step|"ngt_test_qa{env=\"\($env)\", project=\"\($project|gsub("\"";"\\\""))\", operation=\"test\", spec=\"\($spec|gsub("\"";"\\\""))\", scenario=\"\($scen|gsub("\"";"\\\""))\", step=\"\($step|gsub("\"";"\\\""))\", result=\"\(.result.status)\"}  \(.result.executionTime)"' | \
    # Send to the metric server
    curl -v --data-binary @- $PUSH_GATE/metrics/job/ngt
    name=`uname -n`
    timestamp=`date +%s`
    result=`jq -r '.executionStatus' $REPORT/result.json`
    echo "prometrics_exec{name=\"$name\", result=\"$result\"} $timestamp" | curl --data-binary @- $PUSH_GATE/metrics/job/ngt_prometrics
    cp -r /ngt-test-qa/ngt-test-qa/reports/html-report/*  /root/ngt-test-qa/HttpShared/
    cp -r /ngt-test-qa/ngt-test-qa/reports/html-report /root/ngt-test-qa/HttpShared/html-report.$DATE    
    # usermod -u 101 /root/ngt-test-qa/HttpShared/
    cd /root/ngt-test-qa/HttpShared/ & chown 101:101 -R *


else
    echo "File $FILE does not exist."
fi
