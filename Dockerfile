FROM maven:3.6.0-jdk-10

WORKDIR /ngt-test-qa/
EXPOSE 9001
RUN apt-get update
ENV PUSH_GATE="3.12.101.215:9091"

ENV TZ="America/El_Salvador"
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y --no-install-recommends \
    jq curl git chromium vim nano

RUN curl -Ssl https://downloads.gauge.org/stable | sed 's/latest/15176631/g' | sh -s -- --location=/usr/sbin/
RUN gauge install java --version 0.7.1
RUN gauge install json-report
RUN gauge install html-report --version 4.0.6

COPY ./bin/ /ngt-test-qa/bin/

#Tasker will make sure it runs every 5 minutes
RUN chmod a+x /ngt-test-qa/bin/*
RUN git clone https://testqahtc:htc.2020@gitlab.com/testqahtc/ngt-test-qa.git

RUN useradd -ms /bin/bash ngnix
USER ngnix
RUN echo user created
USER root
CMD ["sh", "-c", "/ngt-test-qa/bin/entrypoint.sh"]
